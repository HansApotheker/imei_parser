require 'imei_parser'

RSpec.describe ImeiParser, "#parse" do

  it "parses valid date and status" do

    parser = ImeiParser.new('013977000323877', 'https://selfsolve.apple.com/agreementWarrantyDynamic.do')

    html = "<html lang='en-US'>
      <head></head>
      <body>
        <div id='hardware' class='status green'>
          <h3 id='hardware-true'>Repairs and Service Coverage: Active (limits apply)</h3>
          <h3 id='hardware-false' style='display: none'></h3>
          <h3 id='hardware-covered' style='display: none'></h3>
          <p id='hardware-text'>Your product is covered for eligible hardware repairs and service under AppleCare+.
            <br>Estimated Expiration Date: August 10, 2016<br>
            <a href='http://support.apple.com/kb/he44'>Learn about Apple's coverage information for your product.</a>
          </p>
        </div>
      </body>
    </html>"

    allow(parser).to receive(:get_doc).and_return(Nokogiri::HTML(html))

    expect(parser.get_status).to eq true
    expect(parser.get_expiration_date).to eq Date.new(2016, 8, 10)

  end

end