require 'nokogiri'
require 'watir-webdriver'
require 'date'

class ImeiParser

  def initialize(imei, url)
    @imei = imei
    @url = url
  end

  def get_status

    doc = get_doc

    if ! doc.css("#hardware-true").first.text.empty?
      true
    else
      false
    end

  end

  def get_expiration_date

    doc = get_doc

    text = doc.css("#hardware-text").first.text

    r = /(January|February|March|April|May|June|July|August|September|October|November|December) (\d{1,2}), (\d{4})/
    if text[r]
      Date.parse(text[r])
    end
  end

  def get_doc

    until @doc
      ff = Watir::Browser.new
      ff.goto(@url)
      ff.text_field(:id => 'sn').set(@imei)
      ff.button(:id => 'warrantycheckbutton').click
      html = ff.html
      ff.close

      @doc = Nokogiri::HTML(html)

    end

    @doc
  end


end